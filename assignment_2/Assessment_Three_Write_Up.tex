\documentclass[11pt, a4paper]{article}
\usepackage[utf8]{inputenc}

\usepackage{geometry}
\geometry{
    a4paper,
    top=30mm,
    bottom=30mm
}

\setlength{\parskip}{\baselineskip}
\setlength{\parindent}{0pt}

\usepackage{makecell}

\usepackage{listings}

\usepackage{graphicx}

\usepackage{pdflscape}

\usepackage{hyperref}

\title{

\begin{center}
  \vspace*{-4cm}\includegraphics[scale=3]{UoL_Logo.jpg}
\end{center}

\vspace*{-4cm}
    CMP2804M Team Software Engineering - \\
    Assessment Item 3 Final Report}

\author{Group 33:\\
    Pawel Bielinski (18679372), Christopher Cogdon (18687297),\\
    Jamie Godwin (18675053), Callum Lancaster (14531914), Shiqi Ma (18689183),\\
    Matthew Murr (17664330)}

\usepackage{fancyhdr}

\pagestyle{fancy}

\lhead{CMP2804M Team Software Engineering – Assessment Item 3 Final Report}
\rhead{}

\lfoot{Group 33: Pawel Bielinski (18679372), Christopher Cogdon (18687297),\\
Jamie Godwin (18675053), Callum Lancaster (14531914), Shiqi Ma (18689183),\\
Matthew Murr (17664330)}
\cfoot{}

\rfoot{\thepage}

\renewcommand{\headrulewidth}{0pt}

\begin{document}

\maketitle

\pagebreak

\tableofcontents

\pagebreak

\section{Introduction}
‘cnatural’ is a programming language based on C++ with a focus on lightweight syntax and easy integration into existing C++ projects. This report presents cnatural alongside ‘cn’, a compiler frontend for the cnatural language that compiles to C++. The process behind the creation of the language and compiler is also discussed.

\section{Software Engineering}
The project during the planning phase had a time scale but the scope and requirements would change during development, so a flexible methodology was needed.  Agile was agreed to be used with a ScrumBan approach due to the nature of the project.  ScrumBan is a hybrid of both Scrum with Kanban, using Scrum structure for development cycles and Kanban flexibility for adapting to new requirements.  As students, ScrumBan was more suitable to dedicate learning early on in the project because it allowed new cards to be pulled in-line with individuals capabilities and with what they were learning.

The Scrum structure was set out through roles being assigned to team members
based on their specialties and Scrum “ceremonies” such as: review,
retrospective, and planning was carried out through weekly team updates and
fortnightly supervisor updates/reviews. Review is where team members give
feedback to the Scrum master (Matt M.) or project supervisor (Charles F.)
making sure that requirements are being met and to assist on any ongoing work
if needed. As students, Retrospective is essential, as a team we’d reflect on
our work together and come up with improvements or learning/training, and try
and apply this for the next iteration.  This could be, referencing the Dragon
Book for Flex \& Bison or O'Reilly as literature for helping others with new cards.  Lastly was planning, we’d assess for the coming iteration what cards have been completed and what card’s next and plan ahead.

The Kanban side of the project is a board with 3 sections: To Do, Doing and
Closed. To-Do is a section with current requirements that need completing and
open to be picked up, a backlog.  Doing, is cards that have been selected and
picked up, currently being worked on. And Closed are cards that have been
completed and sit in a pile of completed requirements. Each card is labeled,
type of card (so test or dev) as well as optional if it's not a priority, flex
\& bison, or C++ for example.

\includegraphics[scale=0.525]{kanban.png}

For configuration management and version control we used GitLab, as represented above, an online repository and project manager.  This included online: interactive Kanban board, code reviewing and merging.  Trello and Jira were options we investigated to keep a separate project management board, both are widely and commercially used in business; however for a single project and a small team it made more sense to not branch out and use GitLab.

\includegraphics[scale=0.375]{gitlab.png}

Each member created branches for the development work and once done and tested, was merged into Master.  All GitLab interaction was done through the Linux terminal CLI, this includes: creating branches, pushing, pulling, merging, and reviewing changes.  The Kanban board was updated at meetings with new cards being pulled and checked off.

The scope changes and additional requirements are listed in the PERT chart beneath:

\includegraphics[scale=0.2]{pert.png}

\pagebreak

\section{Implementation}

Our project, cn, is a compiler frontend for the cnatural language, first
developed by Charles Fox (Fox 2019).
cnatural is
an alternative to C++ which removes certain redundant features from the
language, most notably the requirement for header files. The cn compiler aims
to take cnatural code and compile it to valid C++ code with minimal parsing of
tokens that will be later parsed by the C++ compiler, an approach that echoes
Bjarne Stroustrup's first C++ frontend, cfront, which compiled C++ to C code
(1994).

In general, the purpose of the cnatural language is to reduce redundancy in the
C++ language, and the purpose of the cn compiler is to be a lightweight gap
between cnatural code and a 'full-fat' C++ compiler.

\subsection{Rationale}

The project is inspired by and in some ways a continuation of an existing
Python-based implementation by Charles Fox, also refered to as 'pycn'. In this project, Fox
notes "It would be nice extend c natural with [a] proper parsing compiler to
replace this hacky one" (2019), so the project was started with this goal in mind.
In drafting the new implementation of cnatural, the goal of "creating the most
use for existing C++ developers" was also integrated. With this in mind, some
changes were made from the original specification of cnatural that are detailed
below.

\subsection{Features}

\subsubsection{The cnatural Language}

The conversion from cnatural code to C++ code is reasonably simple: all
expressions in cnatural are valid lines of C++ code. cnatural attempts to only
change the things on the 'edges' of the code, syntax features that determine the structure of
code and code files:

\begin{itemize}
    \item Scope is signified by indentation level rather than by curly
        brackets, and semicolons are replaced with newlines. The idea behind
        this change was spurred by these syntactic features already being
        present in
    \item Header files are removed: the include directive can target the name
        of the cn file as if it were a hpp file. This allows for existing C++
        code to use interfaces from cnatural code as if it were C++ code and
        vice versa, improving integration with existing projects.
\end{itemize}

The above cnatural rules used for the cn compiler takes most
ideas as expressed in pycn, but differs in a few
important ways. cn makes fewer departures from C++, with the purpose of
providing the most use to current C++ developers immediateley, with greater
ease of movement, and so the goal is shifted from "remove ALL redundancy from
the C++ language" (Fox 2019) to make C++ syntax more 'lightweight'.

For example, in Fox's cnatural the contents of each file except for files named
"Main.cpp" are treated as a class with the name of the class being shown in the
name of the file. Whilst the practise of having one class to one file is widely
prevalent in programming practice (which would make the in-file class declaration
redundant), a C++ programmer coming to cnatural might ask how to apply
inheritence or a template to the class without having direct control over the
declaration. Whilst these concerns and others can certainly be worked around,
the decision to keep class declarations part of the expression syntax was taken
to make it easier for C++ code to be ported to cnatural and to avoid creating
new alternative syntax outside the file (e.g. the ':' character used to signify
inheritence in C++ is an illegal character on some filesystems, and might have
to be replaced with an alternative). In the same vein, this version of cnatural
adds support for both tab-based and space-based indentation.

Alternative approaches to be looked into regarding what is and is not
considered part of the cnatural grammar are discussed in the Evaluation
section.

\subsubsection{The cn Compiler}

The broad concept of the cn compiler is as a high level in-between from
cnatural codefiles to C++ code. For each cnatural file, a corresponding cpp and
hpp file is generated, providing the interface and implementation as defined in
the cnatural file, using C++ syntax. The compiler itself is written using the
flex and bison tools, so called 'parser parsers', as used in the Dragon Book.
This allows the lexer and syntax rules to be written using essentially regex
and BNF, meaning the compiler is more accessible and quicker to develop on, as
internal details such as passing tokens from the lexer to the parser are
handled underneath the hood (Paxson 2017).

\begin{figure}[h]
\begin{lstlisting}[language=c++]
// f.cn
int f(int a)
    g(a)

// f.hpp
;int f(int a)
;...

// f.cpp
;int f(int a)
{g(a)
;};...

\end{lstlisting}
\caption{A definition in a cnatural file mapped to hpp and cpp.}
\end{figure}


The Dragon Book
gives 7 stages of a compiler:

\begin{figure}[h]
    \includegraphics[height=10cm]{compiler-stages}
    \caption{Compiler phases (Aho et al., 1986, 10)}
\end{figure}

Our frontend compiler is designed to
be simple and lightweight, only trying to do work around the outside of the
actual expressions, and
contains no steps for generating intermediate code or code optimisation. We
need a simple symbol table to store the names of classes so that member
functions can be written in the output cpp file using the scope resolution
operator.

The compiler as a blackbox as reads the input stream a line at
a time and for each line makes some small adjustment at the start and end of
the line to transform it into valid C++ code. This is reasonably similar to the
internal process: flex reads the largest token is can from the file stream and
passes it to the bison parser. Primarily, the tokens in cn are for items at the
beginning and end of each line, and everything in between is gobbled with a
'catch-all' rule.

\begin{figure}[h]
\begin{lstlisting}[language=c]
[A-Za-z\/].+    {
                    // Will be free()d by bison.
                    yylval.sval = strdup(yytext);
                    return EXPR;
                }
\end{lstlisting}
\caption{The 'catch-all' lexer rule.}
\end{figure}

The matched tokens are then passed to the parser and a syntax tree is generated
(Aho et al., 1986, 49). If the syntax matches one of the rules defined in the
grammar, the corresponding C code is executed (Paxson 2017). In general,
the cnatural language features present in the cn compiler are implemented as
follows:

{\bf Indentation-based Scope} -- cn is designed to work with variable indentation
styles (any number of spaces or tabs) - this is done by counting the number of INDENT
tokens on the first line and storing this value for the rest of the file.
Any future
indentations are divided by this initial value to calculate the actual
indentation level. If the indentation doesn't divide through evenly, an
error is raised for inconsistent indentation. The indentation level of any
line is compared to the previous line: if there is an increase in indent level,
then a corresponding number of '\{' characters are inserted. If it is negative,
'\}' characters are inserted, and if there is no change then no adjustment is
made. % I'm ignoring the case 'if the first line is not minimally indented'

{\bf Expressions} -- Expressions in cnatural are all valid C++ code. In cn,
these are caught using one large catch-all token.
The reasoning behind trying to gather the largest possible chunks into tokens
as possible is both for simplicity and speed - we would like cn to spend as
little time possible looking into the internals of code that the C++ parser
will already be parsing. There are some downsides to using this catch-all token
however, which are explained in the Challenges section below.

{\bf Newline Expression Endings} -- In cnatural a newline is considered
sufficient to end an expression. Were cn to insert a semicolon at the end of
each line however, some cases, for example if statements, would not be
valid C++ as they need to have no semicolon between the statement and the block
of code that will be executed. Whilst we could check for keywords, we want to
avoid looking into the expressions as much as possible, and so cn instead
inserts semicolons at the beginning of each line if necessary. This means we
can check for changes in indentation, and adjust at that time.

{\bf Header Replacement} -- Generating headers is arguably the most difficult
requirement for the cn parser, due to differences in how declarations are
formatted in header files and cpp files that implement the interface exposed in
the header. In general, if a particular line is a declaration that defines the
interface for the file then it needs to
be in the hpp file, and if it is a definition then it needs to also be in the
cpp file. We can mostly implement this without analysing the code itself simply
by checking the indentation differences that will be present between a
declaration and a corresponding implementation. This works
in the case that only function declarations and items within function
declarations contain a change in scope, however in C++ there are other
declarations such as structs and, crucially, classes, that can contain
further declarations important in the interface. In order to write these to the
hpp file, we use an 'indentation floor'. This increases
only when a keyword is in the declaration that can contain further declarations
that are part of the interface.  However, due to our catch-all token mentioned
earlier, we cannot add a rule that checks for these things in the lexer.
Therefore, we extend the check for definitions by storing each expression in a
buffer. If there is an increase in indentation, the buffer is checked using C
std library string functions to see if it contains one of these keywords.


\begin{figure}[h]
\begin{lstlisting}[language=c]
void handleClasses(char const *expr)
{
    if (strncmp(expr, "class ", 6) == 0)
    {
        indentFloor += (indentBlockDepth) ? indentBlockDepth : 1;

        // Move ahead by strlen("class ")
        char const *temp = expr + 6;
        char const *p = temp;
        // Iterate on string until word ends
        do { ++p; } while (*p != '\0' && *p != ' ');
        // Place class name into current floor of
        // functionPrefixes (ie scope)
        int x = indentFloor;
        if (indentBlockDepth) { x /= indentBlockDepth; }
        strncpy(functionPrefixes[x], temp, p - temp);
        functionPrefixes[x][p - temp + 1] = '\0';
        strcat(functionPrefixes[x], "::");
    }
}
\end{lstlisting}
\caption{Function to extract class names}
\end{figure}

\pagebreak

If it
does, the indentation floor is increased and nothing is written to the cpp
file. Otherwise, the buffer is written to the cpp file before the current line.
A further issue faced from the separate cpp and hpp files however is that some
declarations have to look different in the declaration than the definition.
The general class of these is discussed below in the Further
Challenges section, however the case of applying the scope resolution operator
to write the function definitions into the cpp file is special. In order to
solve this, the name of a given class is stored upon the keyword being
discovered (see figure). If a function is defined within the class, on the step of the
buffer being written to the cpp file the class name is inserted before the name
of the function.

\subsection{Further Challenges}

Some challenges faced are already mentioned above, however there are some
especially large inter-related problems faced by the project:

{\bf Differing Declarations in Header and Implementation Files} --

Each definition present in the cn file maps to a declaration in the
hpp file and a definition in the cpp file. However, the declarations in either
file can sometimes be required to be different: the case for functions in
classes has been discussed, but also problematic are templates and default
parameters. Default parameters are a little trickier:
the definition in the cpp file should contain no default parameters if they are
specified in the original declaration, however to do
this requires that cn understands default parameters, which is currently
thwarted by the catch-all token.

\begin{figure}[h]
\begin{lstlisting}[language=c]
f.cpp:5:5: error: default argument given for parameter 1 of
'int a(int)' [-fpermissive]
    5 | int a(int b = 0)
      |     ^
f.cpp:3:5: note: previous specification in 'int a(int)' here
    3 | int a(int b = 0);
      |     ^
f.cpp: In function 'int a(int)':
f.cpp:8:1: warning: no return statement in function returning
non-void [-Wreturn-type]
    8 | }
      | ^
\end{lstlisting}
\caption{Error encoutered when separating definition for default parameters.}
\end{figure}


{\bf Catch All Token} -- The catch-all token is very powerful in utily, but has
a crucial downside: flex's tokenisation system always prioritises the largest
token that it can match (Paxson 2017). This means that any token that would fall under the
catch-all, for example a class keyword token, will be gobbled up. The
alternative is removing the catch-all and creating a more sophisticated
understanding of the underlying C++ syntax. Arguably this is worthwhile, since
although we would like the parser to be as lightweight as possible, it means we
do not have to do analysis through C string functions. The problem arrises
however that it seems that in order to have a more complex understanding of the
syntax, you quickly need to build a complete understanding. This is because if
we were to have a set of tokens and syntax rules to parse a function
declaration, as needed in the case of default arguments for example, we also
need tokens to properly differentiate this from other syntax features. This
would not necessarily be difficult to implement, however C++ is especially
tricky due to the presence of overlapping syntax features. Presently, our
solution is to not include these portions of the C++ language in the cnatural
specification.

{\bf Syntax Ambiguity and Context Sensitivity} -- C++ has a few cases of syntax that can be interpreted
differently depending on context. For cn, this should not matter so much,
because we try not to look into the code itself. If we were to require a more
complex understanding as mentioned above, we would need to be able to
differentiate, for example, a function declaration from an initialisation.
However, consider the following code:
\begin{figure}[h]
\begin{lstlisting}[language=c]
A f((T) * x);
\end{lstlisting}
\caption{Ambiguous C++ expression}
\end{figure}

Is this a function declaration that takes a pointer of type T and returns a
value of type A, an initialisation of a variable f of type A to the
multiplication of T and x, or an initialisation of a variable f of type A to
the value of x casted to type (T*)? It is suprisingly difficult for our parser to
determine this as according to the C and C++ specifications we need to know
things about A, T and x beforehand (Bendersky 2007, Roskind 1991), particularly
using flex, which is designed to parse
'context-free' grammars, only caring about line-by-line syntax rather than
semantics across the file (Bendersky 2007, Roskind 1991, Paxson 2017).
Implementing a complete solution for this is beyond the scope of this project.
For the present, we simply define the cnatural language to not include those
features from C++ that cause large distance between the cpp and hpp
declarations, such as templates and default parameters.

\section{Testing Strategy}

\subsection{First Initial Testing}
The group approached the tests with a guided cn file called Cat.cn, this file allowed for the use of writing cn files from the templated Cat.cn file.  The Cat.cn file approach was necessary for testing as it showed the group what was needed for the .cn to compile for the desired output that was written within the .cn file.  This first file was created with the thought of testing the very first version of the compiler, and therefore not many considerations went into thinking of the structure, the naming conventions or separating each specific feature of the language, this would come later.

The group members had to make a .cpp/.hpp file for the given test, the .hpp file has all the referenced variables and implementations that are included in the .cpp file so they would work together.  The .cpp file will then have the code of the specific test that was written, once these C++ files are completed the group would make the .cn file.  This was created by using a text editor such as VIM, all the necessary cn implementations are added in the original C++ .cpp file as described in the implementation section above; such as curly brackets and semicolons and then the file was saved as .cn.

Now that the .cn file was created it was time to test the file, this was done by running the .cn file through a python script that was created in order to check for errors.  If there were any errors during compiling the .cn through the use of the python script then it wouldn’t compile properly and would say where the error was in order for the group member who is testing their file to change it and the failed test was added to the Kanban cards.  If the compilation of the tests were successful, the python script will output the completed compilation and a given score of the test that correlates how successful the test was. With the successful test of the .cn file that the group member made, they pushed the test into their branch of tests so the test could be documented and prove what sections of many C++ implantations have that the .cn can work with; such as classes, loops and so on.

\pagebreak

\subsubsection{Second Phase of Testing}
As the group concluded the tests from the first initial testing, we had a few struggles and errors that are apparent during the use of the Cat.cn approach.  Therefore, our group ditched the whole Cat.cn approach and started testing individual language features, this process is the exact same as the subsection above but tailored to testing different language features. The Cat.cn file example included many features of the language all included at once, and therefore this would make it harder for the developers to know exactly what caused the parser to throw an error, and would take the time to debug logger.  This therefore warranted a change to make the whole process more effective, and the group moved on to creating smaller tests, with the naming describing exactly what feature they were testing.

The test results looked as follows:

\includegraphics[scale=0.61]{python_cn_test.png}

With different “1”s showing that there was either a compiler error or a possible structure error. The compile errors would be appended to the end of these tests, showing what was wrong with the particular test.

\subsection{Final and Deliberate Use of Testing}
The last stage of testing was conducted by one of the group members (Pawel Bielinski) who was given the task of re-writing our advanced programming module assessment from semester one which was learning the principle of C++ OOP.  This last test was chosen because it fit best with all our current testing methods and from the last semester's assessment the group all had a complete/compiled working program that the group could work with.  This gave our group the opportunity to see if all the testing that was conducted and worked properly with successful compilation from C++ to cn, was ready to be released.  Pawel Bielinski re-wrote this assessment program to cn with the use of Matt Murr's complete C++ assessment program; this re-written assessment was then committed to a GitLab branch that was set up. This test was useful as it tested a variety of features, and showed a more real-world example of coding within our language in comparison to the tests, which only test individual features. This helps to show the readiness of cn for larger-scale projects.

Although the test of this assessment conversion from C++ to cn was slightly incorrect due to some compilation errors that needed to be worked on in later stages of development. Most of the code written in cn compiled and worked, but there were a few sections of the original assessment that would not compile correctly resulting in errors.  Even though we didn’t get a full compilation of the original assessment, the cn files are viewable within GitLab to see how it was rewritten.

\section{Release}
Our project will be released as a  FLOSS projected meaning “Free/Libre and Open Source Software,” we have taken this methodology from Richard Stallman’s work as we believe it will help benefit the project as a whole, by allowing others to implement their ideas into our project. The main location for releases will be GitLab as it allows for the constant development of our project.

The project is officially released as the files are already available due to this project being a FLOSS this also means it will continue to be in development. This is one of the main reasons why we are using GitLab as it allows for the perfect conditions of our project as any users interested in cn and how it works, they are able to download the files and edit themselves they can also add changes to the new branch which can later be added to the master branch so that everyone has access to new updates.

When new users wish to commit to the project they are required to use a new branch that prevents updates from taking place on the master branch before they have gone through the pull request system which requires a certain amount of people to confirm the changes made before adding the changes to the master branch can take place. only certain members are allowed to confirm branch mergers stopping random users from uploading code to a functional branch of development. We also used black-box testing to try and minimize errors in the release of the project. This is also assisted by the many people who will be able to see and have access to make changes and give feedback.

Our project can be found here:\\
\url{https://gitlab.com/cnatural/cn}

Our project writeup, containing the demo video, can be found here:\\
\url{https://gitlab.com/cnatural/writeup}

\pagebreak

\section{Evaluation}
This Team Software Engineering group project from the start was challenging being on the topic of compilers and architectures, the project that our group decided to develop on was ambitious yet interesting.  Even though the project is complete there are a lot of features that could’ve made the project more interesting were cut due to time constraints, the group feels happy with the end outcome of the project, and how much each individual group member has learned during the process of this project.
The subsections below will go into detail on all possible evaluations that the group can justify how this year-long project panned out.

\subsection{Optional Features Culled in Design}
During the late developmental stages of the group project some of the work that certain group members were assigned to were cut out of the project.  One of the main portions that were cut from the project was Syntax highlighter.  The main reason for this is that the C++ highlighter already included all the Syntax that would be used in our compiler making the new cn highlighter redundant as it had fewer features included within it.

The role of the C++ specialist was culled as it ended up not being very important to the project as a whole, as the only contribution needed was in terms of testing – therefore this role ended up not contributing much to the whole project as testing was one of the last parts of the task, however, this could have been more effective if more testing was considered earlier on, and test cases were ready when the cn implementation was finished.  This role could have been put on another task for the meanwhile to maximize the effectiveness of the group.

\subsection{Support for Other Operating Systems}
This group project for Team Software Engineering being compilers and architectures, the group decided that it was best to create the compiler/project on Linux based Operating Systems.  This was chosen because Linux OS has complex yet useful features that will aid our group in order to complete this project, such as committing to GitLab and so on.  Therefore, as of right now cn will only compile on Linux OS correctly and has a variety of issues on other OS systems such as Windows/macOS.  With most successful compilation from C++ to cn, the support can be developed for other Operating Systems in later stages of development after this assessment.  Our group expresses that with cn being an open-source project that it must be viable to use on all platforms for a wider variety of users, with cn only compiling on Linux OS as of now it hinders the practical use of the “open-source” approach to our group’s project. The “PATHMAX” implementation would allow for it to read files on Windows however this has not been tested or supported.

\subsection{Peer Review of Artefact}
During the weekly group meeting with our project supervisor, each iteration of the project was reviewed and feedback was given based on what implementation has been added.  During one of our group meetings our project supervisor insisted that a professor in SoCS view our project that was almost complete.  Feedback was given in a very insightful yet conclusive way that showed the project was a very interesting approach at making C++ more welcoming to programming with the use of cn’s implementations.  This feedback allowed the group to take pride in the project as cn was in the end, designed to be most useful to programmers with the use of our implementations, even if some programmers disagree with the changes applied to C++.

The only negative feedback that was given from this particular individual was cn’s implementation of header files not being needed.  The professor expressed that the use of header files made programming in C++ easier.  We took this advice into consideration but decided to go against it as it was one of the most important features being implemented in cn, as stated above some programmers that code in C++ may disagree with the implementations that cn includes.

\subsection{Further Development of Our Project}
With our group project being ‘open-source’ and released publicly on GitLab, our group wishes to maintain and update the project as much as possible.  This can include further development of including more complex implementations for compiling C++ to cn also with the support of other Operating Systems as mentioned in the subsection above.  Further development could also include one of the group members using this project as their own dissertation project to improve on top of what the group has completed.

With the release of the group’s current version of our project, as a whole most group members are satisfied but this final release could be improved beyond what was expected.  Our group hopes that there are a variety of people that would like to work on this project as it’s publicly on GitLab and open-source, so for those who are interested it’s possible for those to develop further on what our current version has implemented.

One critical development that can be further implemented was some of the culled roles such as ‘Syntax Highlighting’, seeing as it was unnecessary at the time of development the group decided to drop the choice of this implementation on development.  Within the programming community, whether it be for commercial or public use, there are a lot of text editors that have Syntax Highlighting included within that specific text editor.  The further development of a Syntax Highlighter would give a personal adjustment on cn with it being a “new” programming language that has been developed. Finally, an output setting was in development into the console CLI, the “-od” is output direct to where the file is stored which is the default setting. In the GitLab branch ‘kit-output-dir’ was working on settings and help options however time constraints cut this short. This is a quality of life improvement for users.


\pagebreak

\section{Group Work Conclusion}
Once the group decided on the project with all the roles that each member has been assigned, the group collectively agreed on dates for meetings.  These meetings would take place every Thursday each week, then the Thursday after we would meet with our group project supervisor so we could inform our supervisor on what was implemented in each stage of the project, along with critical feedback.

Majority of the group attended the weekly meetings and were satisfied with the outcome of what was discussed in these meetings between all group members or the dates we had met with our group supervisor.  The individual group member that didn’t attend these meetings would be informed what was discussed in that particular meeting, in person or via online messaging such as Discord/Facebook group that was specifically setup for our group during this project for communication to all members.  Our group would also communicate in the Discord group via voice calls in order to keep up to date on the progress and implementation of the project.

Although, with everything going according to plan in the first semester of this group project, the second semester wasn’t as co-ordinated.  The group still had weekly Thursday meetings as a group and with our supervisor but some group members felt left behind on the project as one of the group members (Mathew Murr) had more experience than all of the remaining group members.  Therefore it was difficult to keep up with some of the work our Compiler Developer was doing and therefore contributing to the compiler was difficult for team members who were outside of this role from the beginning.

Another difficulty that some group members had to overcome was the fact that a variety of the roles originally assigned to members were culled, the reason behind this was either time constraints or that it was unnecessary for the implementation of the project.  Therefore individual group members felt “lost” in their contribution towards the project as a whole, this issue was finally settled when the testing phase was in practice.  The individual team members (Pawel Beilinski, Jamie Godwin, Callum Lancaster and Shiqi Ma) were allocated towards the testing phase, creating viable tests for cn to compile correctly.  These tests can be found in each of the individual team members branch within the tests on GitLab

\subsection{Individual Involvement With Percentage Allocation}

\includegraphics[scale=0.95]{grade.PNG}

\pagebreak

\section{References}

Aho, A.V., Ullman, J.D. and Sethi, R. (1986) {\it Compilers: principles,
techniques and tools}. Reading, USA: Addison-Wesley.

Bendersky, E. (2007) {\it The context sensitivity of C's grammar}. Eli
Bendersky. Available from
https://eli.thegreenplace.net/2007/11/24/the-context-sensitivity-of-cs-grammar/
[accessed 17 April 2020].

Fox, C. (2019) {\it cnatural} [software]. Charles Fox. Available from
https://gitlab.com/charles.fox/cnatural [accessed 14 April 2020].

GNU Project (2020) {\it GCC libstdc++} [software]. GNU Project. Available from
https://github.com/gcc-mirror/gcc/tree/master/libstdc\%2B\%2B-v3 [accessed 14
April 2020].

Levine J. (2009) Flex \& Bison. Sebastopol, USA: O’Reilly.

Roskind, J. (1991) {\it A yacc-able C++ 2.1 grammar, and the resulting
ambiguities} Jim Roskind. Available from
https://web.archive.org/web/20070622120718/http://www.cs.utah.edu/research/projects/mso/goofie/grammar5.txt
[accesed 18 April 2020].

Stroustrup, B. (1994) {\it The design and evolution of C++}. Reading, USA:
Addison-Wesley.

Vern Paxson (2017) {\it flex manual page}.


Article title: FLOSS and FOSS - GNU Project - Free Software Foundation Website title: Gnu.org URL: https://www.gnu.org/philosophy/floss-and-foss.en.html

\end{document}
